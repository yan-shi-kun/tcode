package test

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/yan-shi-kun/tcode"
	"gitee.com/yan-shi-kun/tcode/test/tables"
	"testing"
	"time"
)

// 初始化代码生成器和数据库链接
var (
	conf = &tcode.Config{
		Dsn:                   "root:root@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=true&loc=Local",
		DriverName:            "mysql",
		Dialect:               "mysql",
		MaxOpenConns:          50,
		MaxIdleConns:          50,
		ConnMaxLifetimeSecond: 600,
		PrimaryKeyColumnName:  "id",
		DebugSQL:              true,
	}
	_, _, _ = tcode.New(conf)
	ctx     = context.Background()
)
var pkid int32 = 4

func init() {
	// 替换默认日志记录方式,日志函数重写
	/*tcode.FuncLog = func(arbitrarily ...any) {
		// 日志重写
	}
	tcode.FuncSQLLog = func(ms float64, sql string, params []any) {
		// sql日志重写
	}*/
}

// TestInsert 新增,未设置的值将使用类型默认值补充
func TestInsert(t *testing.T) {

	var tb tables.TbTest
	tb.UserId = 199999
	tb.CategoryId = 123
	tb.ArticleCover = "Ar't''icl\\'\\\\'\\'eCover"
	tb.ArticleTitle = "ArticleTitle"
	tb.ArticleContent = "ArticleContent"
	tb.Type = 4
	tb.OriginalUrl = "OriginalUrl"
	tb.IsTop = 1
	tb.IsDelete = 0
	tb.Status = 1
	tb.Sortno = 99
	tb.UpdateTime = time.Now()
	insert, pkid, err := tcode.Insert(ctx, &tb)
	fmt.Println(insert, pkid, err)
	fmt.Println(tb)
}

// TestInsertBatch 批量新增
func TestInsertBatch(t *testing.T) {
	var tbs []tcode.Table
	for i := 0; i < 1000; i++ {
		var tb tables.TbTest
		tb.UserId = int32(i)
		tb.UpdateTime = time.Now()
		tb.ArticleTitle = "测试批量新增123"
		tbs = append(tbs, &tb)
	}
	insert, pkid, err := tcode.InsertBatch(ctx, &tbs)
	fmt.Println(insert, pkid, err)
}

// TestUpdate 更新；忽略每一个空列（类型默认值进行忽略）
func TestUpdate(t *testing.T) {
	var tb tables.TbTest
	tb.Id = 201434
	tb.ArticleTitle = "测试\\'1234"
	update, id, err := tcode.UpdateByPk(ctx, &tb)
	fmt.Println(update, id, err)
}

// TestUpdateNotIgnoredEveryColumn 更新：不忽略任何一列
func TestUpdateNotIgnoredEveryColumn(t *testing.T) {
	var tb tables.TbTest
	tb.Id = pkid
	tb.ArticleTitle = "测试1234"
	update, id, err := tcode.UpdateNotIgnoredEveryColumnByPk(ctx, &tb)
	fmt.Println(update, id, err)
}

func TestSave(t *testing.T) {
	var tb tables.TbTest
	tb.Id = 2023
	tb.ArticleTitle = "测试1234"
	update, id, err := tcode.Save(ctx, &tb)
	fmt.Println(update, id, err)
}

// TestDelete 删除
func TestDelete(t *testing.T) {
	var tb tables.TbTest
	tb.Id = pkid
	affected, id, err := tcode.DeleteByPk(ctx, &tb)
	if err != nil {
		panic(err)
	}
	fmt.Println(affected, id, err)
}

// TestSelectToVar 查询结果映射变量
func TestSelectToVar(t *testing.T) {
	var id int
	var articleTitle string
	err := tcode.NewSqlInfo(ctx, "SELECT id,article_title FROM tb_test WHERE id=?", 1).ToVar(&id, &articleTitle)
	if err != nil {
		panic(err)
	}
	fmt.Println(id, articleTitle)
}

// TestSelect 查询结果映射结构体ToStruct
func TestSelect(t *testing.T) {
	toStruct, err := tcode.Select[*tables.TbTest](ctx).AppendSQL(" WHERE id=?", pkid).ToStruct()
	if err != nil {
		panic(err)
	}
	fmt.Println(toStruct, err)
}

// TestSelect1 查询结果映射为切片
func TestSelect1(t *testing.T) {
	toSlice, err := tcode.Select[*tables.TbTest](ctx).ToSlice(nil)
	if err != nil {
		panic(err)
	}
	slice := *toSlice
	for i := range slice {
		fmt.Println(slice[i])
	}
}

// TestSelect2 查询结果映射为切片
func TestSelect2(t *testing.T) {
	//PossibleQueryEmptyColumn 查询的列可能有空数据（nil）
	toSlice, err := tcode.Select[*tables.TbTest](ctx).PossibleQueryEmptyColumn().ToSlice(nil)
	if err != nil {
		panic(err)
	}
	slice := *toSlice
	for i := range slice {
		fmt.Println(slice[i])
	}
}

// TestSelectToRawTable 没有结构体的情况下，将查询结果映射为原始表
func TestSelectToRawTable(t *testing.T) {
	table, err := tcode.NewSqlInfo(ctx, "select * FROM tb_test").ToRawTable(nil)
	if err != nil {
		panic(err)
	}
	//获取行
	row := table.GetRow(1)
	fmt.Println(row["id"])

	//获取列
	col := table.GetColumnByIndex(2)
	//col := table.GetColumnByName("列明")
	for i := range col {
		fmt.Println(col[i])
	}
}

// TestPage 分页查询
func TestPage(t *testing.T) {
	newPage := tcode.NewPage()
	newPage.PageSize = 2
	newPage.NextPage = 1
	page, err := tcode.Select[*tables.TbTest](ctx, "id,article_title").PossibleQueryEmptyColumn().ToSlice(newPage)
	if err != nil {
		panic(err)
	}
	p := *page
	for i := range p {
		fmt.Println(p[i].Id)
		fmt.Println(p[i].ArticleTitle)
	}
	fmt.Println(newPage)
}

// TestPage2 分页查询;自定义查询总记录数函数
func TestPage2(t *testing.T) {
	newPage := tcode.NewPage()
	newPage.PageSize = 2
	newPage.NextPage = 1
	newPage.FuncCustomTotal = func(total *int) error {
		err := tcode.NewSqlInfo(ctx, "SELECT COUNT(*) FROM tb_test").ToVar(total)
		if err != nil {
			return err
		}
		return nil
	}
	page, err := tcode.Select[*tables.TbTest](ctx, "id,article_title").PossibleQueryEmptyColumn().ToSlice(newPage)
	if err != nil {
		panic(err)
	}
	p := *page
	for i := range p {
		fmt.Println(p[i].Id)
		fmt.Println(p[i].ArticleTitle)
	}
	fmt.Println(newPage)
}

// TestSelect3 默认值查询条件不处理
func TestSelect3(t *testing.T) {
	date, err := time.Parse("2006-01-02 15:04:05", "0001-01-01 00:00:00") //不处理
	//date = date.Add(time.Second) 增加一秒
	toStruct, err := tcode.Select[*tables.TbTest](ctx).PossibleQueryEmptyColumn().AppendSQL(" WHERE update_time <= ?", date).ToSlice(nil)
	fmt.Println(err)
	s := *toStruct
	for i := range s {
		fmt.Println(s[i].CreateTime)
	}
}

// TestSelect4 查询条件，in,like
func TestSelect4(t *testing.T) {
	ids := []int32{pkid}
	cids := []int{186}
	slice, err := tcode.Select[*tables.TbTest](ctx).AppendSQL("WHERE id in (?) AND category_id IN (?) and  article_title like ?", ids, cids, "%测试%").ToSlice(nil)
	if err != nil {
		panic(err)
	}
	s := *slice
	for i := range s {
		fmt.Println(s[i])
	}
}

// TestSelect5
func TestSelect5(t *testing.T) {
	table, err := tcode.NewSqlInfo(ctx, "select * FROM tb_test").ToRawTable(nil)
	if err != nil {
		panic(err)
	}
	//获取行
	row := table.GetRow(0)
	fmt.Println(row["article_title"])
}

// TestSelect6
func TestSelect6(t *testing.T) {
	table, err := tcode.NewSqlInfo(ctx, "select * FROM tb_test WHERE id=?", 2).ToRawTable(nil)
	if err != nil {
		panic(err)
	}
	fmt.Println(table.GetRow(0))
}

// TestTransaction 使用事务
func TestTransaction(t *testing.T) {
	err := tcode.Transaction(ctx, func(ctx context.Context) error {
		var tb tables.TbTest
		tb.UserId = 199999
		tb.CategoryId = 123
		tb.ArticleCover = "rollback"
		tb.ArticleTitle = "ArticleTitle"
		tb.ArticleContent = "ArticleContent"
		tb.Type = 4
		tb.OriginalUrl = "OriginalUrl"
		tb.IsTop = 1
		tb.IsDelete = 0
		tb.Status = 1
		tb.Sortno = 99
		tb.UpdateTime = time.Now()
		insert, id, err := tcode.Insert(ctx, &tb)
		fmt.Println(insert, id, err)

		err2 := tcode.Transaction(ctx, func(ctx context.Context) error {
			var tb tables.TbTest
			tb.UserId = 199999
			tb.CategoryId = 123
			tb.ArticleCover = "commit"
			tb.ArticleTitle = "ArticleTitle"
			tb.ArticleContent = "ArticleContent"
			tb.Type = 4
			tb.OriginalUrl = "OriginalUrl"
			tb.IsTop = 1
			tb.IsDelete = 0
			tb.Status = 1
			tb.Sortno = 99
			tb.UpdateTime = time.Now()
			insert, id, err := tcode.Insert(ctx, &tb)
			fmt.Println(insert, id, err)
			return nil
		})
		fmt.Println(err2)

		insert, id, err = tcode.Insert(ctx, &tb)
		fmt.Println(insert, id, err)

		return errors.New("rollback")
	})
	fmt.Println(err)
}

// TestDebugSQL 开启调试sql,会将参数值拼接好的完整sql打印至控制台，用户开发阶段调试复杂sql，可直接在sql控制台执行的sql
func TestDebugSQL(t *testing.T) {
	conf = &tcode.Config{
		Dsn:                   "root:root@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=true&loc=Local",
		DriverName:            "mysql",
		Dialect:               "mysql",
		MaxOpenConns:          50,
		MaxIdleConns:          50,
		ConnMaxLifetimeSecond: 600,
		PrimaryKeyColumnName:  "id",
		DebugSQL:              true,
	}
	_, _, _ = tcode.New(conf)
	ids := []int32{pkid}
	cids := []int{186}
	slice, err := tcode.Select[*tables.TbTest](context.Background()).AppendSQL("WHERE id in (?) AND category_id IN (?) and  article_title like ?", ids, cids, "%测试%").ToSlice(nil)
	if err != nil {
		panic(err)
	}
	s := *slice
	for i := range s {
		fmt.Println(s[i])
	}
}
