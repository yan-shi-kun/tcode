package tables

import (
	"gitee.com/yan-shi-kun/tcode"

	"time"
)

const TableNameTbTest = "test.tb_test"

// TbTest 测试表
type TbTest struct {
	Id             int32     `json:"id"`             // Id
	UserId         int32     `json:"userId"`         // UserId 作者
	CategoryId     int32     `json:"categoryId"`     // CategoryId 文章分类
	ArticleCover   string    `json:"articleCover"`   // ArticleCover 文章缩略图
	ArticleTitle   string    `json:"articleTitle"`   // ArticleTitle 标题
	ArticleContent string    `json:"articleContent"` // ArticleContent 内容
	Type           int8      `json:"type"`           // Type 文章类型 1原创 2转载 3翻译
	OriginalUrl    string    `json:"originalUrl"`    // OriginalUrl 原文链接
	IsTop          int8      `json:"isTop"`          // IsTop 是否置顶 0否 1是
	IsDelete       int8      `json:"isDelete"`       // IsDelete 是否删除  0否 1是
	Status         int8      `json:"status"`         // Status 状态值 1公开 2私密 3评论可见
	Sortno         int32     `json:"sortno"`         // Sortno 自定义排序
	CreateTime     time.Time `json:"createTime"`     // CreateTime 发表时间
	UpdateTime     time.Time `json:"updateTime"`     // UpdateTime 更新时间
}

func (receiver *TbTest) TableName() string {
	return TableNameTbTest
}
func (receiver *TbTest) RawColumnContainer(columns ...string) []any {
	container := make([]any, len(columns))
	for i := range columns {
		switch columns[i] {
		case "id":
			container[i] = &receiver.Id
		case "user_id":
			container[i] = &receiver.UserId
		case "category_id":
			container[i] = &receiver.CategoryId
		case "article_cover":
			container[i] = &receiver.ArticleCover
		case "article_title":
			container[i] = &receiver.ArticleTitle
		case "article_content":
			container[i] = &receiver.ArticleContent
		case "type":
			container[i] = &receiver.Type
		case "original_url":
			container[i] = &receiver.OriginalUrl
		case "is_top":
			container[i] = &receiver.IsTop
		case "is_delete":
			container[i] = &receiver.IsDelete
		case "status":
			container[i] = &receiver.Status
		case "sortno":
			container[i] = &receiver.Sortno
		case "create_time":
			container[i] = &receiver.CreateTime
		case "update_time":
			container[i] = &receiver.UpdateTime
		default:
			container[i] = new(any)
			tcode.FuncLog("warn: not matched to column")
		}
	}
	return container
}
func (receiver *TbTest) NewTable() tcode.Table {
	return &TbTest{
		Id:             receiver.Id,
		UserId:         receiver.UserId,
		CategoryId:     receiver.CategoryId,
		ArticleCover:   receiver.ArticleCover,
		ArticleTitle:   receiver.ArticleTitle,
		ArticleContent: receiver.ArticleContent,
		Type:           receiver.Type,
		OriginalUrl:    receiver.OriginalUrl,
		IsTop:          receiver.IsTop,
		IsDelete:       receiver.IsDelete,
		Status:         receiver.Status,
		Sortno:         receiver.Sortno,
		CreateTime:     receiver.CreateTime,
		UpdateTime:     receiver.UpdateTime,
	}
}
func (receiver *TbTest) NewInstance() tcode.Table {
	return &TbTest{}
}
func (receiver *TbTest) CopyFrom(src tcode.Table) {
	if source, ok := src.(*TbTest); ok {
		receiver.Id = source.Id
		receiver.UserId = source.UserId
		receiver.CategoryId = source.CategoryId
		receiver.ArticleCover = source.ArticleCover
		receiver.ArticleTitle = source.ArticleTitle
		receiver.ArticleContent = source.ArticleContent
		receiver.Type = source.Type
		receiver.OriginalUrl = source.OriginalUrl
		receiver.IsTop = source.IsTop
		receiver.IsDelete = source.IsDelete
		receiver.Status = source.Status
		receiver.Sortno = source.Sortno
		receiver.CreateTime = source.CreateTime
		receiver.UpdateTime = source.UpdateTime
	}
}

var ColumnNamesTbTest = []string{"id", "user_id", "category_id", "article_cover", "article_title", "article_content", "type", "original_url", "is_top", "is_delete", "status", "sortno", "create_time", "update_time"}

func (receiver *TbTest) Columns() []string {
	return ColumnNamesTbTest
}
