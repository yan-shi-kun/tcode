package benchmark

import (
	"context"
	"database/sql"
	"gitee.com/yan-shi-kun/tcode"
	"gitee.com/yan-shi-kun/tcode/test/tables"
	"testing"
	"time"
)

// 初始化代码生成器和数据库链接
var (
	conf = &tcode.Config{
		Dsn:                   "root:root@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=true&loc=Local",
		DriverName:            "mysql",
		Dialect:               "mysql",
		MaxOpenConns:          50,
		MaxIdleConns:          50,
		ConnMaxLifetimeSecond: 600,
		PrimaryKeyColumnName:  "id",
	}
	actuators, _, _ = tcode.New(conf)
	ctx             = context.Background()
)

var db *sql.DB

// 原始方式
func init() {
	tcode.FuncLog = func(arbitrarily ...any) {}                      //忽略日志
	tcode.FuncSQLLog = func(ms float64, sql string, params []any) {} //忽略日志

	db, _ = sql.Open(conf.DriverName, conf.Dsn)
	db.SetConnMaxLifetime(time.Second * time.Duration(conf.ConnMaxLifetimeSecond))
	db.SetMaxOpenConns(conf.MaxOpenConns)
	db.SetMaxIdleConns(conf.MaxIdleConns)
	err := db.Ping()
	if err != nil {
		panic(err)
	}
}
func BenchmarkRawQuerySql(b *testing.B) {
	var id int
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		queryContext, _ := db.QueryContext(context.Background(), "SELECT id FROM tb_test WHERE id=?", 1)
		for queryContext.Next() {
			err := queryContext.Scan(&id)
			if err != nil {
				panic(err)
			}
		}
		err := queryContext.Close()
		if err != nil {
			panic(err)
		}
	}
}
func BenchmarkTCodeQuerySql(b *testing.B) {
	var id int
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		err := tcode.Select[*tables.TbTest](ctx, "id").AppendSQL("WHERE id=?", 1).ToVar(&id)
		if err != nil {
			panic(err)
		}
	}
}
