package tcode

type page struct {
	TotalCount      int `json:"totalCount"`
	TotalPage       int `json:"totalPage"`
	CurrentPage     int `json:"currentPage"`
	NextPage        int `json:"nextPage"`
	PageSize        int `json:"pageSize"`
	FuncCustomTotal func(total *int) error
}

func NewPage() *page {
	p := page{}
	p.NextPage = 1
	p.PageSize = 10
	return &p
}

func (page *page) setTotalCount(total int) {
	page.CurrentPage = page.NextPage
	page.TotalCount = total
	page.TotalPage = (page.TotalCount + page.PageSize - 1) / page.PageSize
	if 1 > page.CurrentPage || page.CurrentPage > page.TotalPage {
		page.CurrentPage = 1
	}
	page.NextPage = page.CurrentPage + 1
	if 1 > page.NextPage || page.NextPage >= page.TotalPage {
		page.NextPage = 1
	}
}
