package tcode

import (
	"context"
	"testing"
	"time"
)

// 初始化代码生成器和数据库链接
var (
	conf = &Config{
		Dsn:                   "root:root@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=true&loc=Local",
		DriverName:            "mysql",
		Dialect:               "mysql",
		MaxOpenConns:          50,
		MaxIdleConns:          50,
		ConnMaxLifetimeSecond: 600,
		PrimaryKeyColumnName:  "id",
		DebugSQL:              false,
	}
	_, codeConstructor, _ = New(conf)
	ctx                   = context.Background()
)

// TestGenAll 生成当前库所有表
func TestGenAll(t *testing.T) {
	info, err := codeConstructor.ToAllStructInfo(ctx)
	if err != nil {
		panic(err)
	}
	for i := range info {
		err = WriteStruct(ctx, info[i])
		if err != nil {
			panic(err)
		}
	}
	time.Sleep(time.Second)
}

// TestGenOtherAll 生成其它库所有表
func TestGenOtherAll(t *testing.T) {
	info, err := codeConstructor.ToAllStructInfoOtherDb(ctx, "库名")
	if err != nil {
		panic(err)
	}
	for i := range info {
		err = WriteStruct(ctx, info[i])
		if err != nil {
			panic(err)
		}
	}
	time.Sleep(time.Second)
}

// TestGenSingeTable 生成指定一个表
func TestGenSingeTable(t *testing.T) {
	info, err := codeConstructor.ToStructInfo(ctx, "库名.表明", "表注释")
	if err != nil {
		panic(err)
	}
	err = WriteStruct(ctx, info)
	if err != nil {
		panic(err)
	}
	time.Sleep(time.Second)
}

// TestGenOtherDir 生成到其它(绝对路径)目录；默认生成到当前目录(./) 目录结构（库名/包名/*）
func TestGenOtherDir(t *testing.T) {
	info, err := codeConstructor.ToStructInfo(ctx, "库名.表明", "表注释")
	if err != nil {
		panic(err)
	}
	err = WriteStructTo(ctx, info, "D://Desktop//")
	if err != nil {
		panic(err)
	}
	time.Sleep(time.Second)
}
