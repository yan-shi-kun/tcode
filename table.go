package tcode

type Table interface {
	TableName() string
	RawColumnContainer(columns ...string) []any
	NewTable() Table
	NewInstance() Table
	CopyFrom(src Table)
	Columns() []string
}
