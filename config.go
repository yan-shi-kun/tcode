package tcode

import "database/sql"

type Config struct {
	Dsn                    string
	DriverName             string
	Dialect                string
	MaxOpenConns           int
	MaxIdleConns           int
	ConnMaxLifetimeSecond  int
	DB                     *sql.DB
	DefaultTxOptions       *sql.TxOptions
	PrimaryKeyColumnName   string //用于统一主键列，所有表的主键列名称必须为此值，否则无法使用代码生成器
	SkipDefaultTransaction bool   //是否跳过默认（增，删，改）事务
	PackageName            string //代码生成器生产代码所使用的包名
	DebugSQL               bool   //是否开启调试sql,会将参数值拼接好的完整sql打印至控制台，用户开发阶段调试复杂sql，可直接在sql控制台执行的sql
}
type confString string

var confMap = make(map[confString]*Config)
