package tcode

import (
	"context"
)

type oracleDbParse int8

func (*oracleDbParse) currentDbName(ctx context.Context) (string, error) {
	//TODO implement me
	panic("implement me")
}

func (*oracleDbParse) columnTypeParse(filedInfo *fieldInfo) error {
	//TODO implement me
	panic("implement me")
}

func (*oracleDbParse) parseFields(ctx context.Context, fullyTableName string) ([]*fieldInfo, error) {
	//TODO implement me
	panic("implement me")
}

func (*oracleDbParse) selectTableName(ctx context.Context) ([]string, error) {
	//TODO implement me
	panic("implement me")
}

func (*oracleDbParse) ToStructInfo(ctx context.Context, fullyTableName, tableComment string) (*structInfo, error) {
	//TODO implement me
	panic("implement me")
}

func (*oracleDbParse) ToAllStructInfo(ctx context.Context) ([]*structInfo, error) {
	//TODO implement me
	panic("implement me")
}

func (*oracleDbParse) ToAllStructInfoOtherDb(ctx context.Context, dbName string) ([]*structInfo, error) {
	//TODO implement me
	panic("implement me")
}
