package tcode

import (
	"fmt"
	"testing"
	"time"
)

// Test_rawTable_ConvertStruct 转换结构体
func Test_rawTable_ConvertStruct(t *testing.T) {
	test1 := struct {
		Id           int32
		ArticleTitle string
		CreateTime   time.Time
		UpdateTime   time.Time
		Sortno       int32
	}{}
	table, err := NewSqlInfo(ctx, "select * FROM tb_test where id=?", 1014).ToRawTable(nil)
	if err != nil {
		fmt.Println(err)
	}
	err = Convert(table, &test1)
	fmt.Println(err, test1)
}

// Test_rawTable_Convert_Slice 转换结构体切片
func Test_rawTable_Convert_Slice(t *testing.T) {
	var test1 []struct {
		Id           int32
		ArticleTitle string
		CreateTime   time.Time
		UpdateTime   time.Time
		Sortno       int32
	}
	table, err := NewSqlInfo(ctx, "select * FROM tb_test where id=?", 1014).ToRawTable(nil)
	if err != nil {
		fmt.Println(err)
	}
	err = Convert(table, &test1)
	fmt.Println(err, test1)
}

// Test_rawTable_Convert_Var_Slice 转换基本类型切片
func Test_rawTable_Convert_Var_Slice(t *testing.T) {
	var test1 []int
	table, err := NewSqlInfo(ctx, "select id FROM tb_test where id=?", 1014).ToRawTable(nil)
	if err != nil {
		fmt.Println(err)
	}
	err = Convert(table, &test1)
	fmt.Println(err, test1)
}

// Test_rawTable_Convert_Var 转换基本类型
func Test_rawTable_Convert_Var(t *testing.T) {
	// 第0行0列
	var test1 time.Time
	table, err := NewSqlInfo(ctx, "select update_time FROM tb_test where id=?", 1014).ToRawTable(nil)
	if err != nil {
		fmt.Println(err)
	}
	err = Convert(table, &test1)
	fmt.Println(err, test1)
}

// Test_rawTable_Convert_Slice rawTable分页查询
func Test_rawTable_Convert_Slice_QueryPage(t *testing.T) {
	var test1 []struct {
		Id           int32
		ArticleTitle string
		CreateTime   time.Time
		UpdateTime   time.Time
		Sortno       int32
	}
	newPage := NewPage()
	table, err := NewSqlInfo(ctx, "select * FROM tb_test").ToRawTable(newPage)
	if err != nil {
		fmt.Println(err)
	}
	err = Convert(table, &test1)
	fmt.Println(newPage, err, test1)
}

// Test_rawTable_ConvertColIndex_Var_Slice 转换基本类型切片
func Test_rawTable_ConvertColIndex_Var_Slice(t *testing.T) {
	var test1 []int
	table, err := NewSqlInfo(ctx, "select id,user_id FROM tb_test where id=?", 1014).ToRawTable(nil)
	if err != nil {
		fmt.Println(err)
	}
	err = ConvertColIndex(table, 1, &test1)
	fmt.Println(err, test1)
}

// Test_rawTable_ConvertColIndex_Var_Slice 转换基本类型切片
func Test_rawTable_ConvertColName_Var_Slice(t *testing.T) {
	var test1 int
	table, err := NewSqlInfo(ctx, "select id,user_id FROM tb_test where id=?", 1014).ToRawTable(nil)
	if err != nil {
		fmt.Println(err)
	}
	err = ConvertColName(table, "id", &test1)
	fmt.Println(err, test1)
}
